from django.shortcuts import render,get_object_or_404,redirect
from appalz.models import WorkingDayRecord,Project,Task,UserProfile,Absence,Client,Vehicle,Comment,Festive,ContactPerson,AbsenceType,ProjectType,ProjectStatus,ContractType,ClientType,ProjectHours,TaskType,FavouriteClient,FavouriteProject,VehicleComment
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from datetime import datetime,date,timedelta
from rest_framework.authtoken.models import Token
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework import viewsets, mixins, status, renderers
from rest_framework.decorators import action
from appalz import serializers
from rest_framework_simplejwt.views import TokenObtainPairView
import xlwt
import xlsxwriter
import io
from django.http import HttpResponse, FileResponse

# Create your views here.

def UserLogin(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return HttpResponse('logeado correctamente')
        else:
            return HttpResponse("Credenciales de acceso incorrectas.")
    else:
        return render(request,'registration/login.html',{})

def UserLogout(request):
    logout(request)
    return redirect('appalz:vehicle')

def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('appalz:trabajador_detalle',pk=request.user.id)
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'appalz/trabajador_changepassword.html', {'form':form})

#############################

class UserViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.UserSerializer
    queryset = User.objects.all()

    @action(detail=True, methods=['post'])
    def set_password(self,serializer,pk=None):
        user=self.get_object()
        serializer = serializers.ChangePasswordSerializer(data=self.request.data)
        if serializer.is_valid():
            user.set_password(self.request.data['new_password'])
            user.save()
            return Response({'status': 'password set'})
        else:
            return Response(serializers.errors,status=status.HTTP_400_BAD_REQUEST)

class FestiveViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.FestiveSerializer
    queryset = Festive.objects.order_by('date')

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

    def list(self,request):
        festivesqueryset = Festive.objects.order_by('date').filter(date__year=datetime.now().year)
        festives = serializers.FestiveSerializer(festivesqueryset,many=True)
        absencesqueryset = Absence.objects.filter(user=request.user,status='C',startdate__year=datetime.now().year)
        title = ['date','type','financial_year']
        simpleabsencequeryset = []
        for absence in absencesqueryset:
            start = absence.startdate
            end = absence.enddate
            type = absence.type.description
            financial_year = absence.financial_year
            delta = end - start
            for i in range(delta.days+1):
                day = start + timedelta(days=i)
                data = [day,type,financial_year]
                simpleabsencequeryset.append(dict(zip(title,data)))
        absences = serializers.SimpleAbsenceSerializer(simpleabsencequeryset,many=True)
        context = {
            'festives': festives.data,
            'absences': absences.data
        }
        return Response(context)

    def extractdates(self,request):
        user = request.data['user']
        financial_year = request.data['financial_year']
        festivesqueryset = Festive.objects.order_by('date').filter(date__year=financial_year)
        festives = serializers.FestiveSerializer(festivesqueryset,many=True)
        absencesqueryset = Absence.objects.filter(user_id=user,status='C',startdate__year=financial_year)
        title = ['date','type','financial_year']
        simpleabsencequeryset = []
        for absence in absencesqueryset:
            start = absence.startdate
            end = absence.enddate
            type = absence.type.description
            financial_year = absence.financial_year
            delta = end - start
            for i in range(delta.days+1):
                day = start + timedelta(days=i)
                data = [day,type,financial_year]
                simpleabsencequeryset.append(dict(zip(title,data)))
        absences = serializers.SimpleAbsenceSerializer(simpleabsencequeryset,many=True)
        context = {
            'festives': festives.data,
            'absences': absences.data
        }
        return Response(context)

class VehicleViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.VehicleSerializer
    queryset = Vehicle.objects.order_by('created_date')

    def perform_create(self,serializer):
        serializer.save(created_by=self.request.user)

    def retrieve(self, request, pk=None):
        vehiclequeryset = Vehicle.objects.all()
        vehicleobject = get_object_or_404(vehiclequeryset, pk=pk)
        vehicle = serializers.VehicleSerializer(vehicleobject)
        taskqueryset = Task.objects.filter(vehicle_id=vehicleobject.id).order_by('-date')
        tasks = serializers.TaskSerializer(taskqueryset,many=True)
        commentsqueryset = VehicleComment.objects.filter(vehicle_id=vehicleobject.id).order_by('last_update')
        comments = serializers.VehicleCommentSerializer(commentsqueryset,many=True)
        context = {
                    'vehicle':vehicle.data,
                    'tasks':tasks.data,
                    'comments':comments.data
                    }
        return Response(context)

class ContactPersonViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.ContactPersonSerializer
    queryset = ContactPerson.objects.all()

    def perform_create(self,serializer):
        serializer.save(created_by=self.request.user)

class ClientTypeViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.ClientTypeSerializer
    queryset = ClientType.objects.order_by('created_date')

    def perform_create(self,serializer):
        serializer.save(created_by=self.request.user)

class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.order_by('-code')

        # mapping serializer into the action
    serializer_classes = {
        'list': serializers.NewClientSerializer,
        'create': serializers.ClientSerializer,
        'update': serializers.ClientSerializer,
        'partial_update': serializers.ClientSerializer
    }
    default_serializer_class = serializers.NewProjectSerializer # Your default serializer

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action, self.default_serializer_class)

    def create(self,request,*args,**kwargs):
        serializer = self.get_serializer(data=request.data, many=isinstance(request.data, list))
        serializer.is_valid()
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, pk=None):
        clientqueryset = Client.objects.all()
        clientobject = get_object_or_404(clientqueryset,pk=pk)
        client = serializers.ClientSerializer(clientobject)
        projectqueryset = Project.objects.filter(client_id=clientobject.id)
        contactpersonqueryset= ContactPerson.objects.filter(client_id=clientobject.id)
        projects = serializers.ProjectSerializer(projectqueryset,many=True)
        contactpersons = serializers.ContactPersonSerializer(contactpersonqueryset,many=True)
        context = {
                    'client':client.data,
                    'projects':projects.data,
                    'contactpersons':contactpersons.data
                    }
        return Response(context)

    def list(self,request):
        clients = Client.objects.select_related('type').values('id','name','code','cif','street','cp','town','province','active','type','telephone',
                                                                'invoicing_email','created_date', 'last_update','created_by','type__description')
        serializer = serializers.NewClientSerializer(clients,many=True)
        
        return Response(serializer.data)

class UserProfileViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.UserProfileSerializer
    queryset = UserProfile.objects.order_by('id')

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user, password='Alzineta2020')

    def perform_update(self,serializer):
        instance = serializer.save()

class IsManagerViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.SimpleUserProfileSerializer
    queryset = UserProfile.objects.filter(is_manager=True).order_by('id')

class AbsenceTypeViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.AbsenceTypeSerializer
    queryset = AbsenceType.objects.order_by('created_date')

    def perform_create(self,serializer):
        serializer.save(created_by=self.request.user)

class AbsenceViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.AbsenceSerializer
    queryset = Absence.objects.order_by('-startdate')

class ContractTypeViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.ContractTypeSerializer
    queryset = ContractType.objects.order_by('created_date')

    def perform_create(self,serializer):
       serializer.save(created_by=self.request.user)

class ProjectTypeViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.ProjectTypeSerializer
    queryset = ProjectType.objects.order_by('created_date')

    def perform_create(self,serializer):
        serializer.save(created_by=self.request.user)

class ProjectStatusViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.ProjectStatusSerializer
    queryset = ProjectStatus.objects.order_by('created_date')

    def perform_create(self,serializer):
        serializer.save(created_by=self.request.user)

class ProjectViewSet(viewsets.ModelViewSet):
    #serializer_class = serializers.ProjectSerializer
    queryset = Project.objects.order_by('-budget_date')
    
    # mapping serializer into the action
    serializer_classes = {
        'list': serializers.NewProjectSerializer,
        'create': serializers.ProjectSerializer,
        'update': serializers.ProjectSerializer,
        'partial_update': serializers.ProjectSerializer
    }
    default_serializer_class = serializers.NewProjectSerializer # Your default serializer

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action, self.default_serializer_class)

    def create(self,request,*args,**kwargs):
        serializer = self.get_serializer(data=request.data, many=isinstance(request.data, list))
        serializer.is_valid()
        serializer.save(created_by=self.request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, pk=None):
        projectqueryset = Project.objects.all()
        projectobject = get_object_or_404(projectqueryset,pk=pk)
        project = serializers.ProjectSerializer(projectobject)
        taskqueryset = Task.objects.filter(project_id=projectobject.id)
        commentqueryset= Comment.objects.filter(project_id=projectobject.id).order_by('-last_update')
        projecthourqueryset= ProjectHours.objects.filter(project_id=projectobject.id)
        tasks = serializers.TaskSerializer(taskqueryset,many=True)
        comments = serializers.CommentSerializer(commentqueryset,many=True)
        projecthours = serializers.ProjectHourSerializer(projecthourqueryset,many=True)
        context = {
                    'project':project.data,
                    'tasks':tasks.data,
                    'projecthours':projecthours.data,
                    'comments':comments.data
                    }
        return Response(context)
    
    def list(self,request):
        projects = Project.objects.select_related('client','type','status','manager').values('id', 'name', 'description', 'code', 'client', 'address', 
                'street', 'cp', 'town', 'province','type', 'status', 'start_date', 'budget_date', 'end_date', 'invoiced', 
                'manager', 'created_date', 'created_by','last_update','client__name','type__description','status__description','manager__username')
        serializer = serializers.NewProjectSerializer(projects,many=True)
        
        return Response(serializer.data)

class FavouriteProjectViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.FavouriteProjectSerializer
    queryset = FavouriteProject.objects.all()

    def perform_create(self,serializer):
        serializer.save(created_by=self.request.user)

    def list(self, request):
        queryset = FavouriteProject.objects.filter(user=request.user)
        serializer = serializers.FavouriteProjectSerializer(queryset, many=True)
        return Response(serializer.data)

class TaskTypeViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.TaskTypeSerializer
    queryset = TaskType.objects.all()

class WorkingDayRecordViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.WorkingDayRecordSerializer
    queryset = WorkingDayRecord.objects.all()

    def list(self,request):
        today = datetime.now().today()
        queryset = WorkingDayRecord.objects.filter(user=request.user,date=today,isovertimehour=False).order_by('id')
        serializer = serializers.WorkingDayRecordSerializer(queryset,many=True)
        return Response(serializer.data)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user,date=datetime.now().date(),start=datetime.now().time())

    def partial_update(self,request,pk=None):
        workingdayrecord = self.get_object()
        serializer = self.serializer_class(workingdayrecord,data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save(stop=datetime.now().time())
        return Response(serializer.data)

    @action(detail=False, methods=['post'])
    def set_overtimehour(self,serializer,pk=None):
        serializer = serializers.WorkingDayRecordSerializer(data=self.request.data)
        if serializer.is_valid():
            serializer.save(user=self.request.user)
            return Response({'status': 'overtimehour saved'})
        else:
            return Response(serializers.errors,status=status.HTTP_400_BAD_REQUEST)

class MonthlyRecordViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.MonthlyRecordSerializer
    queryset = WorkingDayRecord.objects.all()

    def list(self,request):
        user_pk = self.request.query_params.get('user', None)
        month = self.request.query_params.get('month', None)
        year = self.request.query_params.get('year', None)
        recordsqueryset = WorkingDayRecord.objects.filter(user_id=user_pk, date__month=month, date__year=year).order_by('date', 'start')
        overtimequeryset = WorkingDayRecord.objects.filter(user_id=user_pk, date__month=month, date__year=year,isovertimehour=True).order_by('date')
        user = get_object_or_404(User,pk=user_pk)
        title = ['date','start_morning','end_morning','start_afternoon','end_afternoon']
        records = []
        date = ''
        for i in range(1,32):
            data = [i,'','','','','','']
            for record in recordsqueryset:
                if record.date.day == i and record.isovertimehour == False:
                    if record.date == date:
                        start_afternoon = record.start.replace(microsecond=0)
                        if record.stop is not None: 
                            end_afternoon = record.stop.replace(microsecond=0)
                            start = datetime.strptime(str(start_afternoon),'%H:%M:%S')
                            end = datetime.strptime(str(end_afternoon),'%H:%M:%S')
                            delta = end - start
                            aft_hours = delta.seconds / 3600
                            aft_hours = round(aft_hours,2)
                            hours += aft_hours
                        else:
                            aft_hours = 0
                            hours += aft_hours
                            total += aft_hours
                        data = [i,start_morning,end_morning,start_afternoon,end_afternoon,hours,hoursextra]
                    else:
                        date = record.date
                        start_morning = record.start.replace(microsecond=0)
                        start_afternoon = ''
                        end_afternoon = ''
                        start_extra = ''
                        end_extra = ''
                        total = 0
                        hoursextra = 0
                        if record.stop is not None:
                            end_morning = record.stop.replace(microsecond=0)
                            start = datetime.strptime(str(start_morning),'%H:%M:%S')
                            end = datetime.strptime(str(end_morning),'%H:%M:%S')
                            delta = end - start
                            hours = delta.seconds / 3600
                            hours = round(hours,2)
                        else:
                            hours = 0
                            total = hours
                            end_morning = ''
                        data = [i,start_morning,end_morning,start_afternoon,end_afternoon,hours,hoursextra]
                if record.date.day == i and record.isovertimehour == True:
                    date = record.date
                    start_extra = record.start.replace(microsecond=0)
                    end_extra = record.stop.replace(microsecond=0)
                    startextra = datetime.strptime(str(start_extra),'%H:%M:%S')
                    endextra = datetime.strptime(str(end_extra),'%H:%M:%S')
                    deltaextra = endextra - startextra
                    hoursextra = deltaextra.seconds / 3600
                    hoursextra = round(hoursextra,2)
                    if data[0] == record.date.day:
                        data[6] = hoursextra 
                    else: 
                        data = [i,'','','','',hoursextra,hoursextra]    
            records.append(data)
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output,{'default_date_format':
                                                  'dd/mm/yy'})
        date_format = workbook.add_format({'num_format': 'dd/mm/yy'})
        time_format = workbook.add_format({'num_format': 'hh:mm','border': True })
        ws = workbook.add_worksheet(user.first_name + ' '+ user.last_name)
        bold = workbook.add_format({'bold': True})
        title = workbook.add_format({'bold': True, 'center_across':True})
        little = workbook.add_format({'italic': True, 'size': 6})
        boldborder = workbook.add_format({'bold': True,'border': True, 'center_across':True})
        border = workbook.add_format({'border': True , 'center_across':True})
        rightborder = workbook.add_format({'right': True , 'center_across':True})
        righttopborder = workbook.add_format({'right': True, 'top': True, 'center_across':True})
        ws.merge_range('A1:I1', "REGISTRO DIARIO DE JORNADA EN TRABAJADORES A TIEMPO PARCIAL/COMPLETO",title)
        ws.merge_range('A2:E2', "EMPRESA",boldborder)
        ws.merge_range('F2:I2', "TRABAJADOR",boldborder)
        ws.merge_range('A3:C3', "Nombre o Razón Social",boldborder)
        ws.merge_range('D3:E3', "Alzineta, S.L.",border)
        ws.merge_range('A4:C4', "CIF",boldborder)
        ws.merge_range('D4:E4', "B96246012",border)
        ws.write('F3', "Nombre",boldborder)
        ws.merge_range('G3:I3', user.first_name + ' '+ user.last_name,border)
        ws.write('F4', "NIF",boldborder)
        ws.merge_range('G4:I4', user.userprofiles.dni,border)
        ws.merge_range('A6:C6', "PERIODO DE LIQUIDACIÓN:",bold)
        ws.write('D6', month + '-' + year)
        ws.merge_range('A9:A10','DÍA',border)
        ws.merge_range('B9:C9','MAÑANA',border)
        ws.merge_range('D9:E9','TARDE',border)
        ws.write('B10','Entrada',border)
        ws.write('C10','Salida',border)
        ws.write('D10','Entrada',border)
        ws.write('E10','Salida',border)
        ws.write('F9','Horas',righttopborder)
        ws.write('F10','Jornada',rightborder)
        ws.write('G9','Horas',righttopborder)
        ws.write('G10','Extras',rightborder)
        row_num = 10

        for record in records:
            ws.write(row_num,0,record[0],border)
            ws.write(row_num,1,record[1],time_format)
            ws.write(row_num,2,record[2],time_format)
            ws.write(row_num,3,record[3],time_format)
            ws.write(row_num,4,record[4],time_format)
            if record[1] != '':
                ws.write_formula(row_num,5,'=ROUND(IF(C{0}=0,0,24*(C{0}-B{0}))+IF(E{0}=0,0,24*(E{0}-D{0})),2)'.format(str(row_num+1)),border)
                ws.write(row_num,6,record[6],border)
            else:
                ws.write(row_num,5,0,border)
                ws.write(row_num,6,0,border)
            row_num += 1
        ws.write_formula(row_num,5,'=SUM(F11:F{})'.format(str(row_num)))
        ws.write_formula(row_num,6,'=SUM(G11:G{})'.format(str(row_num)))
        ws.merge_range('A{0}:E{1}'.format(str(row_num+1),str(row_num+1)), "TOTAL HORAS MES",border)
        ws.merge_range('A{0}:I{1}'.format(str(row_num+3),str(row_num+3)), "Registro basado en la obligación establecida en el art 35.5 del Texto Refundido del Estatuto de los Trabajadores (RDL 2/2015 de 23 de Octubre)", little)
        ws.merge_range('A{0}:D{1}'.format(str(row_num+5),str(row_num+5)), "FIRMA DE LA EMPRESA",bold)
        ws.merge_range('F{0}:I{1}'.format(str(row_num+5),str(row_num+5)), "FIRMA DEL TRABAJADOR",bold)
        ws.merge_range('A{0}:C{1}'.format(str(row_num+6),str(row_num+9)), "",border)
        ws.merge_range('F{0}:H{1}'.format(str(row_num+6),str(row_num+9)), "",border)

        workbook.close()
        output.seek(0)
        filename = 'REGISTRO_HORARIO_LABORAL.xlsx'
        response = FileResponse(
            output,
            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        )
        response['Content-Disposition'] = 'attachment; filename=%s' % filename
        return response

class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = serializers.MyTokenObtainPairSerializer

class LoggedInUserView(viewsets.ModelViewSet):
    serializer_class = serializers.UserProfileSerializer
    queryset = UserProfile.objects.all()

    def list(self, request):
        loggeduser = get_object_or_404(UserProfile,user_id=request.user.id)
        serializer = serializers.UserProfileSerializer(loggeduser)
        return Response(serializer.data)

class TaskViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.TaskSerializer
    queryset = Task.objects.all()

    def perform_create(self,serializer):
        serializer.save(created_by=self.request.user)

class CommentViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.CommentSerializer
    queryset = Comment.objects.all()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

class ProjectHourViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.ProjectHourSerializer
    queryset = ProjectHours.objects.all()

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)

class VehicleCommentViewSet(viewsets.ModelViewSet):
     serializer_class = serializers.VehicleCommentSerializer
     queryset = VehicleComment.objects.order_by('last_update')

     def perform_create(self,serializer):
         serializer.save(user=self.request.user)

class AllMonthlyRecordViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.MonthlyRecordSerializer
    queryset = WorkingDayRecord.objects.all()

    def list(self,request):
        month = self.request.query_params.get('month', None)
        year = self.request.query_params.get('year', None)
        recordsqueryset = WorkingDayRecord.objects.filter(date__month=month, date__year=year).order_by('date', 'start')
        overtimequeryset = WorkingDayRecord.objects.filter(date__month=month, date__year=year,isovertimehour=True).order_by('date')
        users = User.objects.filter(is_active=True)

        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output,{'default_date_format':
                                                    'dd/mm/yy'})
        date_format = workbook.add_format({'num_format': 'dd/mm/yy'})
        time_format = workbook.add_format({'num_format': 'hh:mm','border': True })
       
        bold = workbook.add_format({'bold': True})
        title = workbook.add_format({'bold': True, 'center_across':True})
        little = workbook.add_format({'italic': True, 'size': 6})
        boldborder = workbook.add_format({'bold': True,'border': True, 'center_across':True})
        border = workbook.add_format({'border': True , 'center_across':True})
        rightborder = workbook.add_format({'right': True , 'center_across':True})
        righttopborder = workbook.add_format({'right': True, 'top': True, 'center_across':True})

        for user in users:
            ws = workbook.add_worksheet(user.first_name + ' '+ user.last_name)
            records = []
            date = ''
            for i in range(1,32):
                data = [i,'','','','','','']
                for record in recordsqueryset:
                    if record.date.day == i and record.isovertimehour == False and record.user.id == user.id:
                        if record.date == date:
                            start_afternoon = record.start.replace(microsecond=0)
                            if record.stop is not None: 
                                end_afternoon = record.stop.replace(microsecond=0)
                                start = datetime.strptime(str(start_afternoon),'%H:%M:%S')
                                end = datetime.strptime(str(end_afternoon),'%H:%M:%S')
                                delta = end - start
                                aft_hours = delta.seconds / 3600
                                aft_hours = round(aft_hours,2)
                                hours += aft_hours
                            else:
                                aft_hours = 0
                                hours += aft_hours
                                total += aft_hours
                            data = [i,start_morning,end_morning,start_afternoon,end_afternoon,hours,hoursextra]
                        else:
                            date = record.date
                            start_morning = record.start.replace(microsecond=0)
                            start_afternoon = ''
                            end_afternoon = ''
                            start_extra = ''
                            end_extra = ''
                            total = 0
                            hoursextra = 0
                            if record.stop is not None:
                                end_morning = record.stop.replace(microsecond=0)
                                start = datetime.strptime(str(start_morning),'%H:%M:%S')
                                end = datetime.strptime(str(end_morning),'%H:%M:%S')
                                delta = end - start
                                hours = delta.seconds / 3600
                                hours = round(hours,2)
                            else:
                                hours = 0
                                total = hours
                                end_morning = ''
                            data = [i,start_morning,end_morning,start_afternoon,end_afternoon,hours,hoursextra]
                    if record.date.day == i and record.isovertimehour == True and record.user.id == user.id:
                        date = record.date
                        start_extra = record.start.replace(microsecond=0)
                        end_extra = record.stop.replace(microsecond=0)
                        startextra = datetime.strptime(str(start_extra),'%H:%M:%S')
                        endextra = datetime.strptime(str(end_extra),'%H:%M:%S')
                        deltaextra = endextra - startextra
                        hoursextra = deltaextra.seconds / 3600
                        hoursextra = round(hoursextra,2)
                        if data[0] == record.date.day:
                            data[6] = hoursextra 
                        else: 
                            data = [i,'','','','',hoursextra,hoursextra]    
                records.append(data)
            ws.merge_range('A1:I1', "REGISTRO DIARIO DE JORNADA EN TRABAJADORES A TIEMPO PARCIAL/COMPLETO",title)
            ws.merge_range('A2:E2', "EMPRESA",boldborder)
            ws.merge_range('F2:I2', "TRABAJADOR",boldborder)
            ws.merge_range('A3:C3', "Nombre o Razón Social",boldborder)
            ws.merge_range('D3:E3', "Alzineta, S.L.",border)
            ws.merge_range('A4:C4', "CIF",boldborder)
            ws.merge_range('D4:E4', "B96246012",border)
            ws.write('F3', "Nombre",boldborder)
            ws.merge_range('G3:I3', user.first_name + ' '+ user.last_name,border)
            ws.write('F4', "NIF",boldborder)
            ws.merge_range('G4:I4', user.userprofiles.dni,border)
            ws.merge_range('A6:C6', "PERIODO DE LIQUIDACIÓN:",bold)
            ws.write('D6', month + '-' + year)
            ws.merge_range('A9:A10','DÍA',border)
            ws.merge_range('B9:C9','MAÑANA',border)
            ws.merge_range('D9:E9','TARDE',border)
            ws.write('B10','Entrada',border)
            ws.write('C10','Salida',border)
            ws.write('D10','Entrada',border)
            ws.write('E10','Salida',border)
            ws.write('F9','Horas',righttopborder)
            ws.write('F10','Jornada',rightborder)
            ws.write('G9','Horas',righttopborder)
            ws.write('G10','Extras',rightborder)
            row_num = 10

            for record in records:
                ws.write(row_num,0,record[0],border)
                ws.write(row_num,1,record[1],time_format)
                ws.write(row_num,2,record[2],time_format)
                ws.write(row_num,3,record[3],time_format)
                ws.write(row_num,4,record[4],time_format)
                if record[1] != '':
                    ws.write_formula(row_num,5,'=ROUND(IF(C{0}=0,0,24*(C{0}-B{0}))+IF(E{0}=0,0,24*(E{0}-D{0})),2)'.format(str(row_num+1)),border)
                    ws.write(row_num,6,record[6],border)
                else:
                    ws.write(row_num,5,0,border)
                    ws.write(row_num,6,0,border)
                row_num += 1
            ws.write_formula(row_num,5,'=SUM(F11:F{})'.format(str(row_num)))
            ws.write_formula(row_num,6,'=SUM(G11:G{})'.format(str(row_num)))
            ws.merge_range('A{0}:E{1}'.format(str(row_num+1),str(row_num+1)), "TOTAL HORAS MES",border)
            ws.merge_range('A{0}:I{1}'.format(str(row_num+3),str(row_num+3)), "Registro basado en la obligación establecida en el art 35.5 del Texto Refundido del Estatuto de los Trabajadores (RDL 2/2015 de 23 de Octubre)", little)
            ws.merge_range('A{0}:D{1}'.format(str(row_num+5),str(row_num+5)), "FIRMA DE LA EMPRESA",bold)
            ws.merge_range('F{0}:I{1}'.format(str(row_num+5),str(row_num+5)), "FIRMA DEL TRABAJADOR",bold)
            ws.merge_range('A{0}:C{1}'.format(str(row_num+6),str(row_num+9)), "",border)
            ws.merge_range('F{0}:H{1}'.format(str(row_num+6),str(row_num+9)), "",border)

        workbook.close()
        
        output.seek(0)
        filename = 'REGISTRO_HORARIO_LABORAL.xlsx'
        response = FileResponse(
            output,
            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        )
        response['Content-Disposition'] = 'attachment; filename=%s' % filename
        return response

class AllRecordsViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.WorkingDayRecordSerializer
    queryset = WorkingDayRecord.objects.filter(date__year = datetime.now().year, date__month__in = [datetime.now().month ,datetime.now().month -1 ]).order_by('-id')




