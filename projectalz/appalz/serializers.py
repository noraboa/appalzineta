from rest_framework import serializers
from appalz.models import WorkingDayRecord,Project,Task,UserProfile,Absence,Client,Vehicle,Comment,Festive,ContactPerson,AbsenceType,ProjectType,ProjectStatus,ContractType,ClientType,ProjectHours,TaskType,FavouriteClient,FavouriteProject, TaskPhotos,VehicleComment
from appalz import models
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from datetime import datetime, date

class ChangePasswordSerializer(serializers.Serializer):
    new_password = serializers.CharField(required=True)

class ContractTypeSerializer(serializers.ModelSerializer):
    """ Serialize a contract type """
    created_by = serializers.StringRelatedField(many=False)

    class Meta:
        model = ContractType
        fields = ('id','description', 'created_date', 'last_update', 'created_by')
        extra_kwargs = {'created_by':{'read_only':True}}

    def create(self,validated_data):
        contracttype = ContractType.objects.create(**validated_data)
        return contracttype

class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(style={'input_type': 'password'},trim_whitespace=False,write_only=True,)
    is_manager = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id','username','first_name','last_name','email','password','is_manager','is_active')
        extra_kwargs = {'username': {'validators': []},}

    def get_is_manager(self,obj):
        is_manager = obj.userprofiles.is_manager
        return is_manager

class SimpleUserProfileSerializer(serializers.ModelSerializer):
    username = serializers.SerializerMethodField()
    user = UserSerializer(many=False)

    class Meta:
        model = UserProfile
        fields = ('id','username','user')

    def get_username(self,obj):
        username = obj.user.username
        return username

class UserProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer(many=False)
    hol_enjoyed = serializers.SerializerMethodField()
    hol_pending = serializers.SerializerMethodField()
    pmat_enjoyed = serializers.SerializerMethodField()
    pmat_pending = serializers.SerializerMethodField()
    hol_enjoyed_lastyear = serializers.SerializerMethodField()
    hol_pending_lastyear = serializers.SerializerMethodField()
    contracttype = serializers.PrimaryKeyRelatedField(many=False,queryset=ContractType.objects.all())
    contracttypename = serializers.SerializerMethodField()
    created_by = serializers.StringRelatedField(many=False)

    class Meta:
        model = UserProfile
        fields = ('id','user', 'dni', 'nuss','street','cp','town','province','telephone','company_startdate',
                  'contract_startdate','contract_enddate','contracttype', 'contracttypename','hol_generated','hol_enjoyed','hol_pending'
                  ,'pmat_generated','pmat_enjoyed','pmat_pending','hol_generated_lastyear','hol_enjoyed_lastyear','hol_pending_lastyear','is_manager','created_date','last_update','created_by'
                  )
        extra_kwargs = {'created_by':{'read_only':True}}

    def get_hol_enjoyed(self,obj):
        absences = Absence.objects.filter(user=obj.user,type_id=1,status='C',financial_year=datetime.now().today().year)
        hol_enjoyed_user = 0
        for absence in absences:
            absence_start = absence.startdate
            absence_end = absence.enddate
            absence_days_delta = absence_end - absence_start
            absence_days = absence_days_delta.days + 1 #aquí tenemos el total de días que dura la ausencia
            hol_enjoyed_user += absence_days
        return hol_enjoyed_user
    
    def get_hol_pending(self,obj):
        hol_pending_user = obj.hol_generated
        absences = Absence.objects.filter(user=obj.user,type_id=1,status='C',financial_year=datetime.now().today().year)
        for absence in absences:
            absence_start = absence.startdate
            absence_end = absence.enddate
            absence_days_delta = absence_end - absence_start
            absence_days = absence_days_delta.days + 1 #aquí tenemos el total de días que dura la ausencia
            hol_pending_user -= absence_days
        return hol_pending_user

    def get_pmat_enjoyed(self,obj):
        absences = Absence.objects.filter(user=obj.user,type_id=2,status='C',financial_year=datetime.now().today().year)
        pmat_enjoyed_user = 0
        for absence in absences:
            absence_start = absence.startdate
            absence_end = absence.enddate
            absence_days_delta = absence_end - absence_start
            absence_days = absence_days_delta.days + 1 #aquí tenemos el total de días que dura la ausencia
            pmat_enjoyed_user += absence_days
        return pmat_enjoyed_user

    def get_pmat_pending(self,obj):
        pmat_pending_user = obj.pmat_generated
        absences = Absence.objects.filter(user=obj.user,type_id=2,status='C',financial_year=datetime.now().today().year)
        for absence in absences:
            absence_start = absence.startdate
            absence_end = absence.enddate
            absence_days_delta = absence_end - absence_start
            absence_days = absence_days_delta.days + 1 #aquí tenemos el total de días que dura la ausencia
            pmat_pending_user -= absence_days
        return pmat_pending_user

    def get_hol_enjoyed_lastyear(self,obj):
        absences = Absence.objects.filter(user=obj.user,type_id=1,status='C',financial_year=(datetime.now().today().year-1))
        hol_enjoyed_user = 0
        for absence in absences:
            absence_start = absence.startdate
            absence_end = absence.enddate
            absence_days_delta = absence_end - absence_start
            absence_days = absence_days_delta.days + 1 #aquí tenemos el total de días que dura la ausencia
            hol_enjoyed_user += absence_days
        return hol_enjoyed_user
    
    def get_hol_pending_lastyear(self,obj):
        hol_pending_user = obj.hol_generated_lastyear if obj.hol_generated_lastyear is not None else 30
        absences = Absence.objects.filter(user=obj.user,type_id=1,status='C',financial_year=(datetime.now().today().year-1))
        for absence in absences:
            absence_start = absence.startdate
            absence_end = absence.enddate
            absence_days_delta = absence_end - absence_start
            absence_days = absence_days_delta.days + 1 #aquí tenemos el total de días que dura la ausencia
            hol_pending_user -= absence_days
        return hol_pending_user

    def get_contracttypename(self,obj):
        contracttypename = obj.contracttype.description
        return contracttypename

    def create(self, validated_data):
        user_validated_data = validated_data.pop('user')
        user = User(
            username=user_validated_data['username'],
            first_name = user_validated_data['first_name'],
            last_name=user_validated_data['last_name'],
            email=user_validated_data['email'],
            is_active=user_validated_data['is_active']
        )
        user.set_password(user_validated_data['password'])
        user.save()
        userprofile = UserProfile.objects.create(
            user = user,
            dni=validated_data['dni'],
            nuss=validated_data['nuss'],
            street=validated_data['street'],
            cp=validated_data['cp'],
            town=validated_data['town'],
            province=validated_data['province'],
            telephone=validated_data['telephone'],
            company_startdate=validated_data['company_startdate'],
            contract_startdate=validated_data['contract_startdate'],
            contract_enddate=validated_data['contract_enddate'],
            contracttype=validated_data['contracttype'],
            hol_generated=validated_data['hol_generated'],
            pmat_generated=validated_data['pmat_generated'],
            hol_generated_lastyear=validated_data['hol_generated_lastyear'],
            is_manager=validated_data['is_manager'],
            created_by=validated_data['created_by'],
        )

        return userprofile

    def update(self,instance,validated_data):
        user = validated_data.get('user')
        instance.user.first_name = user.get('first_name')
        instance.user.last_name = user.get('last_name')
        instance.user.email = user.get('email')
        instance.user.is_active = user.get('is_active')
        instance.user.save()
        instance.dni = validated_data.get('dni')
        instance.nuss = validated_data.get('nuss')
        instance.street = validated_data.get('street')
        instance.cp = validated_data.get('cp')
        instance.town = validated_data.get('town')
        instance.province = validated_data.get('province')
        instance.telephone = validated_data.get('telephone')
        instance.company_startdate = validated_data.get('company_startdate')
        instance.contract_startdate = validated_data.get('contract_startdate')
        instance.contract_enddate = validated_data.get('contract_enddate')
        instance.contracttype = validated_data.get('contracttype')
        instance.hol_generated = validated_data.get('hol_generated')
        instance.pmat_generated = validated_data.get('pmat_generated')
        instance.hol_generated_lastyear = validated_data.get('hol_generated_lastyear')
        instance.is_manager = validated_data.get('is_manager')
        instance.save()
        return instance

class ClientTypeSerializer(serializers.ModelSerializer):
    """ Serialize a client type """
    created_by = serializers.StringRelatedField(many=False)

    class Meta:
        model = ClientType
        fields = ('id','description', 'created_date', 'last_update', 'created_by')
        extra_kwargs = {'created_by':{'read_only':True}}

    def create(self,validated_data):
        clienttype = ClientType.objects.create(**validated_data)
        return clienttype

class ClientSerializer(serializers.ModelSerializer):
    """ Serialize a client  """
    type = serializers.PrimaryKeyRelatedField(many=False, queryset=ClientType.objects.all())
    typename = serializers.SerializerMethodField()
    created_by = serializers.StringRelatedField(many=False)

    class Meta:
        model = Client
        fields = ('id','name','code','cif','street','cp','town','province','active','type','typename','telephone','invoicing_email','created_date', 'last_update','created_by')
        extra_kwargs = {'created_by':{'read_only':True}}

    def create(self,validated_data):
        client = Client.objects.create(**validated_data)
        return client

    def get_typename(self,obj):
        typename = obj.type.description
        return typename

class NewClientSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
    code = serializers.IntegerField()
    cif = serializers.CharField()
    street = serializers.CharField()
    cp = serializers.IntegerField()
    town = serializers.CharField()
    province = serializers.CharField()
    active = serializers.BooleanField()
    type = serializers.IntegerField()
    typename = serializers.CharField(source='type__description')
    invoicing_email = serializers.EmailField()
    telephone = serializers.IntegerField()
    created_date = serializers.DateTimeField()
    last_update = serializers.DateTimeField()
    created_by = serializers.IntegerField()

class FavouriteClientSerializer(serializers.ModelSerializer):
    """ Serialize a favourite client """
    client = serializers.StringRelatedField(many=False)
    user = serializers.StringRelatedField(many=False)

    class Meta:
        model = FavouriteClient
        fields = ('id','client','user','created_date', 'last_update')
        extra_kwargs = {'created_by':{'read_only':True}}

class WorkingDayRecordSerializer(serializers.ModelSerializer):
    """ Serialize a working day record"""
    user = serializers.StringRelatedField(many=False)
    class Meta:
        model = WorkingDayRecord
        fields = ('id','user','date','start', 'stop','comment','isovertimehour','active')
        read_only_fields = ('id' ,)

    def create(self,validated_data):
        workingdayrecord = WorkingDayRecord.objects.create(**validated_data)
        return workingdayrecord

class ProjectTypeSerializer(serializers.ModelSerializer):
    """ Serialize a client type """
    created_by = serializers.StringRelatedField(many=False)

    class Meta:
        model = ProjectType
        fields = ('id','description', 'created_date', 'last_update', 'created_by')
        extra_kwargs = {'created_by':{'read_only':True}}

    def create(self,validated_data):
        projecttype = ProjectType.objects.create(**validated_data)
        return projecttype

class ProjectStatusSerializer(serializers.ModelSerializer):
    """ Serialize a client type """
    created_by = serializers.StringRelatedField(many=False)

    class Meta:
        model = ProjectStatus
        fields = ('id','description', 'created_date', 'last_update', 'created_by')
        extra_kwargs = {'created_by':{'read_only':True}}

    def create(self,validated_data):
        projectstatus = ProjectStatus.objects.create(**validated_data)
        return projectstatus

class ProjectSerializer(serializers.ModelSerializer):
    """ Serialize a project """
    client = serializers.PrimaryKeyRelatedField(many=False,queryset=Client.objects.all())
    type = serializers.PrimaryKeyRelatedField(many=False,queryset=ProjectType.objects.all())
    status  = serializers.PrimaryKeyRelatedField(many=False,queryset=ProjectStatus.objects.all())
    created_by = serializers.StringRelatedField(many=False)
    clientname = serializers.SerializerMethodField()
    typename = serializers.SerializerMethodField()
    statusname = serializers.SerializerMethodField()
    managername = serializers.SerializerMethodField()
    
    class Meta:
        model = Project
        fields = ('id', 'name', 'description', 'code', 'client', 'clientname', 'address', 'street', 'cp', 'town', 'province',
        'type', 'typename', 'status', 'statusname', 'start_date', 'budget_date', 'end_date', 'invoiced', 'manager', 'managername', 'created_date', 'created_by',
        'last_update')
        extra_kwargs = {'created_by':{'read_only':True}}

    def create(self,validated_data):
        project = Project.objects.create(
            name=validated_data['name'],
            description=validated_data['description'],
            code=validated_data['code'],
            client=validated_data['client'],
            address=validated_data['address'],
            street=validated_data['street'],
            cp=validated_data['cp'],
            town=validated_data['town'],
            province=validated_data['province'],
            type=validated_data['type'],
            status=validated_data['status'],
            start_date=validated_data['start_date'],
            budget_date=validated_data['budget_date'],
            end_date=validated_data['end_date'],
            invoiced=validated_data['invoiced'],
            manager=validated_data['manager'],
            created_by=validated_data['created_by']
        )
        return project

    def get_clientname(self,obj):
        clientname = obj.client.name
        return clientname

    def get_managername(self,obj):
        managername = obj.manager.username
        return managername

    def get_typename(self, obj):
        typename = obj.type.description
        return typename

    def get_statusname(self, obj):
        statusname = obj.status.description
        return statusname

class NewProjectSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
    description = serializers.CharField()
    code = serializers.CharField()
    client = serializers.IntegerField()
    clientname = serializers.CharField(source='client__name')
    address = serializers.CharField()
    street = serializers.CharField()
    cp = serializers.IntegerField()
    town = serializers.CharField()
    province = serializers.CharField()
    type = serializers.IntegerField()
    typename = serializers.CharField(source='type__description')
    status = serializers.IntegerField()
    statusname = serializers.CharField(source='status__description')
    start_date = serializers.DateField()
    budget_date = serializers.DateField()
    end_date = serializers.DateField()
    invoiced = serializers.BooleanField()
    manager = serializers.IntegerField()
    managername = serializers.CharField(source='manager__username')
    created_date = serializers.DateTimeField()
    created_by = serializers.IntegerField()
    last_update = serializers.DateTimeField()

class FavouriteProjectSerializer(serializers.ModelSerializer):
    """ Serialize a favourite project """
    project = serializers.PrimaryKeyRelatedField(many=False,queryset=Project.objects.all())
    user = serializers.PrimaryKeyRelatedField(many=False,queryset=User.objects.all())

    class Meta:
        model = FavouriteProject
        fields = ('id','project','user','created_date')


    def create(self,validated_data):
        favouriteproject = FavouriteProject.objects.create(
            project=validated_data['project'],
            user=validated_data['user']
        )
        return favouriteproject

class VehicleSerializer(serializers.ModelSerializer):
    """ Serialize a vehicle """
    created_by = serializers.StringRelatedField(many=False)

    class Meta:
        model = Vehicle
        fields = ('id','name','plate','insurance_company','insurance_type','insurance_expiration_date','insurance_driver','itv_date','created_by','created_date', 'last_update' )
        extra_kwargs = {'created_by':{'read_only':True}}

    def create(self,validated_data):
        vehicle = Vehicle.objects.create(**validated_data)
        return vehicle

class TaskTypeSerializer(serializers.ModelSerializer):
    """ Serialize a client type """
    created_by = serializers.StringRelatedField(many=False)
    projecttype = serializers.PrimaryKeyRelatedField(many=False,queryset=ProjectType.objects.all())
    projecttypename = serializers.SerializerMethodField()

    class Meta:
        model = TaskType
        fields = ('id','description','projecttype','projecttypename','created_by','created_date', 'last_update')
        extra_kwargs = {'created_by':{'read_only':True}}


    def get_projecttypename(self,obj):
        projecttypename = obj.projecttype.description
        return projecttypename 

class TaskSerializer(serializers.ModelSerializer):
    """ Serialize a client type """
    created_by = serializers.StringRelatedField(many=False)
    project = serializers.PrimaryKeyRelatedField(many=False,queryset=Project.objects.all())
    type = serializers.PrimaryKeyRelatedField(many=False,queryset=TaskType.objects.all())
    typename = serializers.SerializerMethodField()
    user = serializers.PrimaryKeyRelatedField(many=False,queryset=User.objects.all())
    username = serializers.SerializerMethodField()
    vehicle = serializers.PrimaryKeyRelatedField(many=False, queryset=Vehicle.objects.all())
    vehiclename = serializers.SerializerMethodField()
    projectname = serializers.SerializerMethodField()

    class Meta:
        model = Task
        fields = ('id','name','description','date','start_plan','end_plan','start_real','end_real','project', 'projectname','type','typename','user','username','albaran','done','vehicle','vehiclename','equipment','epis','created_date','created_by','last_update')
        extra_kwargs = {'created_by': {'read_only': True}}

    def create(self,validated_data):
        task = Task.objects.create(**validated_data)
        return task

    def get_projectname(self,obj):
        projectname = obj.project.name
        return projectname

    def get_username(self,obj):
        username = obj.user.username
        return username

    def get_typename(self,obj):
        typename = obj.type.description
        return typename

    def get_vehiclename(self,obj):
        if obj.vehicle is not None:
            return obj.vehicle.name
        else:
            return "none"

class AbsenceTypeSerializer(serializers.ModelSerializer):
    """ Serialize a client type """
    created_by = serializers.StringRelatedField(many=False)

    class Meta:
        model = AbsenceType
        fields = ('id','description','created_by','created_date', 'last_update')
        extra_kwargs = {'created_by':{'read_only':True}}

    def create(self,validated_data):
        absencetype = AbsenceType.objects.create(**validated_data)
        return absencetype

class AbsenceSerializer(serializers.ModelSerializer):
    """ Serialize an absence """
    user = serializers.PrimaryKeyRelatedField(many=False,queryset=User.objects.all())
    type = serializers.PrimaryKeyRelatedField(many=False, queryset=AbsenceType.objects.all())
    typename = serializers.SerializerMethodField()
    username = serializers.SerializerMethodField()
    duration = serializers.SerializerMethodField()

    class Meta:
        model = Absence
        fields = ('id','user','username','startdate','starttime','enddate','endtime','duration','type','typename','status','financial_year','comment','created_date', 'last_update')
        extra_kwargs = {'status':{'default':'P'},
                        'starttime':{'default':'00:00'},
                        'endtime':{'default':'23:59'}
                        }

    def create(self,validated_data):
        absence = Absence.objects.create(**validated_data)
        return absence

    def get_username(self,obj):
        username = obj.user.username
        return username

    def get_typename(self,obj):
        typename = obj.type.description
        return typename
    
    def get_duration(self,obj):
        absence_start = obj.startdate
        absence_end = obj.enddate
        absence_days_delta = absence_end - absence_start
        duration = absence_days_delta.days + 1 #aquí tenemos el total de días que dura la ausencia
        return duration

class SimpleAbsenceSerializer(serializers.Serializer):
    """ Serialzie an absence with fewer fields"""
    date = serializers.DateField()
    type = serializers.CharField(max_length=200)
    financial_year = serializers.IntegerField()

class ContactPersonSerializer(serializers.ModelSerializer):
    """ Serialize a contact person """
    created_by = serializers.StringRelatedField(many=False)
    client = serializers.PrimaryKeyRelatedField(many=False,queryset=Client.objects.all())
    class Meta:
        model = ContactPerson
        fields = ('id','client','firstname','lastname','telephone','email','role','created_by','created_date', 'last_update')
        extra_kwargs = {'created_by':{'read_only':True}}

    def create(self, validated_data):
        contactperson = ContactPerson.objects.create(**validated_data)
        return contactperson

class CommentSerializer(serializers.ModelSerializer):
    """ Serialize a comment """
    project = serializers.PrimaryKeyRelatedField(many=False,queryset=Project.objects.all())
    task = serializers.StringRelatedField(many=False)
    user = serializers.PrimaryKeyRelatedField(many=False, queryset=User.objects.all())
    username = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        fields = ('id', 'project', 'task', 'user', 'username', 'comment', 'created_date', 'last_update')

    def create(self, validated_data):
        comment = Comment.objects.create(**validated_data)
        return comment

    def get_username(self,obj):
        username = obj.user.username
        return username

class ProjectHourSerializer(serializers.ModelSerializer):
    """ Serialize a client type """
    project = serializers.PrimaryKeyRelatedField(many=False,queryset=Project.objects.all())
    created_by = serializers.StringRelatedField(many=False)
    real_hours = serializers.SerializerMethodField()
    class Meta:
        model = ProjectHours
        fields = ('id','project','month','year','budg_hours','real_hours', 'created_date','last_update','created_by')
        extra_kwargs = {'created_by': {'read_only': True}}

    def get_real_hours(self,obj):
        tasks = Task.objects.filter(project=obj.project,date__month=obj.month,date__year=obj.year,done=True)
        real_hours = 0
        for task in tasks:
            task_start = datetime.strptime(str(task.start_plan),'%H:%M:%S')
            task_end = datetime.strptime(str(task.end_plan),'%H:%M:%S')
            task_delta = task_end - task_start
            task_hours = task_delta.seconds / 3600
            real_hours += task_hours
        return real_hours

    def create(self, validated_data):
        projecthour = ProjectHours.objects.create(**validated_data)
        return projecthour

class TaskPhotosSerializer(serializers.ModelSerializer):
    """ Serialize a client type """
    project = serializers.StringRelatedField(
        many=False
    )
    user = serializers.StringRelatedField(
        many=False
    )
    class Meta:
        model = TaskPhotos
        fields = [
            'id','project','user','type','photo', 'created_date','last_update'
        ]
        read_only_fields = ('id' ,)

class FestiveSerializer(serializers.ModelSerializer):
    """ Serialize a festive)"""

    class Meta:
        model = Festive
        fields = ('id','date','description')

    def create(self,validated_data):
        festive = Festive.objects.create(**validated_data)
        return festive

class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)
        refresh = self.get_token(self.user)
        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)
        # Add extra responses here
        data['ismanager'] = self.user.userprofiles.is_manager

        return data

class MonthlyRecordSerializer(serializers.Serializer):
    user = serializers.IntegerField()
    month = serializers.IntegerField()
    year = serializers.IntegerField()

class VehicleCommentSerializer(serializers.ModelSerializer):
    """" Serialize a vehicle comment """
    vehicle = serializers.PrimaryKeyRelatedField(many=False,queryset=Vehicle.objects.all())
    user = serializers.PrimaryKeyRelatedField(many=False, queryset=User.objects.all())
    username = serializers.SerializerMethodField()

    class Meta:
        model = VehicleComment
        fields = ('id', 'vehicle', 'user', 'username', 'comment', 'created_date', 'last_update')

    def create(self, validated_data):
        comment = VehicleComment.objects.create(**validated_data)
        return comment

    def get_username(self,obj):
        username = obj.user.username
        return username
