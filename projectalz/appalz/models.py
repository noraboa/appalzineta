from django.db import models
from django.utils import timezone
from django.urls import reverse
from django.contrib.auth.models import User
from django.contrib import auth
from datetime import datetime
# Create your models here.

class ContractType(models.Model):
    description = models.CharField(max_length=100)
    created_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User,on_delete=models.CASCADE)

    def __str__(self):
        return self.description

class UserProfile(models.Model):
    user = models.OneToOneField(User,related_name='userprofiles',on_delete=models.CASCADE)
    profile_photo = models.ImageField(upload_to='profile_pics',blank=True,null=True)
    dni = models.CharField(max_length=9,blank=True,null=True)
    nuss = models.BigIntegerField(blank=True,null=True)
    street = models.CharField(max_length=200,blank=True,null=True)
    cp = models.PositiveIntegerField(blank=True,null=True)
    town = models.CharField(max_length=100,blank=True,null=True)
    province = models.CharField(max_length=100,blank=True,null=True)
    telephone = models.PositiveIntegerField(blank=True,null=True)
    company_startdate = models.DateField(blank=True,null=True)
    contract_startdate = models.DateField(blank=True,null=True)
    contract_enddate = models.DateField(blank=True,null=True)
    contracttype = models.ForeignKey(ContractType,on_delete=models.CASCADE)
    hol_generated = models.PositiveIntegerField(blank=True,null=True)
    pmat_generated = models.PositiveIntegerField(blank=True,null=True)
    hol_generated_lastyear = models.PositiveIntegerField(blank=True,null=True)
    is_manager = models.BooleanField(default=False)
    dni_photo = models.ImageField(upload_to='dni_pics',blank=True,null=True)
    drivinglicens_photo = models.ImageField(upload_to='dlic_pics',blank=True,null=True)
    created_date = models.DateField(auto_now_add=True)
    last_update = models.DateField(auto_now=True)
    created_by = models.ForeignKey(User,related_name='userprofile_created_by',on_delete=models.CASCADE)

    def __str__(self):
        return "@{}".format(self.user.username)

class ClientType(models.Model):
    description = models.CharField(max_length=100)
    created_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User,on_delete=models.CASCADE)

    def __str__(self):
        return self.description

class Client(models.Model):
    name = models.CharField(max_length=100)
    code = models.PositiveIntegerField()
    cif = models.CharField(max_length=10)
    street = models.CharField(max_length=200,blank=True,null=True)
    cp = models.PositiveIntegerField(blank=True,null=True)
    town = models.CharField(max_length=100,blank=True,null=True)
    province = models.CharField(max_length=100,blank=True,null=True)
    active = models.BooleanField(default=False,blank=True,null=True)
    type = models.ForeignKey(ClientType,on_delete=models.CASCADE,blank=True,null=True)
    invoicing_email = models.EmailField(blank=True,null=True)
    telephone = models.PositiveIntegerField(blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User,related_name='client_created_by',on_delete=models.CASCADE)

    def __str__(self):
        return self.name

class FavouriteClient(models.Model):
    client = models.ForeignKey(Client,on_delete=models.CASCADE)
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "client {} usuario {}".format(self.client,self.user)

class WorkingDayRecord(models.Model):
    user = models.ForeignKey(User,related_name='registrojornadas',on_delete=models.CASCADE)
    date = models.DateField(blank=True,null=True)
    start = models.TimeField(blank=True,null=True)
    stop = models.TimeField(blank=True,null=True)
    comment = models.CharField(max_length=200,blank=True,null=True)
    isovertimehour = models.BooleanField(default=False)
    active = models.BooleanField(default=False)

    def __str__(self):
        return "Fichaje de {} del día {}".format(self.user, self.date)

class ProjectType(models.Model):
    description = models.CharField(max_length=100)
    created_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User,on_delete=models.CASCADE)

    def __str__(self):
        return self.description

class ProjectStatus(models.Model):
    description = models.CharField(max_length=100)
    created_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User,on_delete=models.CASCADE)

    def __str__(self):
        return self.description

class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=300,blank=True,null=True)
    code = models.CharField(max_length=10,blank=True,null=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE,blank=True,null=True)
    address = models.CharField(max_length=200,blank=True,null=True)
    street = models.CharField(max_length=200,blank=True,null=True)
    cp = models.PositiveIntegerField(blank=True,null=True)
    town = models.CharField(max_length=100,blank=True,null=True)
    province = models.CharField(max_length=100,blank=True,null=True)
    type = models.ForeignKey(ProjectType,on_delete=models.CASCADE)
    status = models.ForeignKey(ProjectStatus,on_delete=models.CASCADE)
    start_date = models.DateField(blank=True,null=True)
    budget_date = models.DateField(blank=True,null=True)
    end_date = models.DateField(blank=True,null=True)
    invoiced = models.BooleanField(default=False)
    manager = models.ForeignKey(User,related_name='projectsmanager',on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User,related_name='projectscreador',on_delete=models.CASCADE)
    last_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

class FavouriteProject(models.Model):
    project = models.ForeignKey(Project,on_delete=models.CASCADE)
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('project','user')

    def __str__(self):
        return "Project {} usuario {}".format(self.project,self.user)

class Vehicle(models.Model):
    name = models.CharField(max_length=100)
    plate = models.CharField(max_length=7)
    insurance_company = models.CharField(max_length=100,blank=True,null=True)
    insurance_type = models.CharField(max_length=100,blank=True,null=True)
    insurance_expiration_date = models.DateField(blank=True,null=True)
    insurance_driver = models.CharField(max_length=100,blank=True,null=True)
    itv_date = models.DateField(blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User,on_delete=models.CASCADE)
    last_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

class TaskType(models.Model):
    description = models.CharField(max_length=100)
    projecttype = models.ForeignKey(ProjectType,on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User,on_delete=models.CASCADE)

    def __str__(self):
        return self.description

class Task(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=300)
    date = models.DateField()
    start_plan = models.TimeField(blank=True,null=True)
    end_plan = models.TimeField(blank=True,null=True)
    start_real = models.TimeField(blank=True,null=True)
    end_real = models.TimeField(blank=True,null=True)
    project = models.ForeignKey(Project,related_name="tasks",on_delete=models.CASCADE)
    type = models.ForeignKey(TaskType,related_name='tasktypes',on_delete=models.CASCADE)
    user = models.ForeignKey(User,related_name='tasks',on_delete=models.CASCADE)
    albaran = models.ImageField(blank=True,null=True,upload_to='documents/%Y7%m/%d')
    done = models.BooleanField(default=False)
    vehicle = models.ForeignKey(Vehicle,on_delete=models.CASCADE,blank=True,null=True)
    equipment = models.CharField(max_length=100,blank=True,null=True)
    epis = models.CharField(max_length=100,blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User,related_name='task',on_delete=models.CASCADE)
    last_update = models.DateTimeField(auto_now=True)

    def horas_planificadas(self):
        self.hours_plan = str(datetime.strptime(str(self.end_plan),"%H:%M:%S") - datetime.strptime(str(self.start_plan),"%H:%M:%S"))
        self.save()

    def horas_reales(self):
        self.hours_real = str(datetime.strptime(str(self.end_real),"%H:%M:%S") - datetime.strptime(str(self.start_real),"%H:%M:%S"))
        self.save()

    def end_task(self):
        self.done = True
        self.start_real = self.start_plan
        self.end_real = self.end_plan
        self.save()

    def get_absolute_url(self):
        return reverse("appalz:Project_detalle",kwargs={'pk':self.project.pk})

    def __str__(self):
        return self.name

class AbsenceType(models.Model):
    description = models.CharField(max_length=100)
    created_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User,on_delete=models.CASCADE)

    def __str__(self):
        return self.description

class Absence(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    startdate = models.DateField()
    starttime = models.TimeField(default='00:00')
    enddate = models.DateField()
    endtime = models.TimeField(default='23:59')
    type = models.ForeignKey(AbsenceType,on_delete=models.CASCADE)
    P = 'Pending'
    C = 'Confirmed'
    D = 'Denied'
    STATUS_CHOICES =[
    ('P','Pending'),
    ('C','Confirmed'),
    ('D','Denied'),
    ]
    status = models.CharField(max_length=15,choices=STATUS_CHOICES)
    financial_year = models.PositiveSmallIntegerField()
    comment = models.CharField(max_length=200,blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} de tipo {}".format(self.user.username,self.type)

    def get_absolute_url(self):
        return reverse("appalz:ausencia_detalle",kwargs={'pk':self.pk})

class ContactPerson(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    firstname = models.CharField(max_length=100)
    lastname = models.CharField(max_length=100,blank=True,null=True)
    telephone = models.PositiveIntegerField(blank=True,null=True)
    email = models.EmailField(blank=True,null=True)
    role = models.CharField(max_length=100)
    created_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User,on_delete=models.CASCADE)

    def __str__(self):
        return "{}-{}".format(self.lastname,self.client.name)

class Comment(models.Model):
    project = models.ForeignKey(Project,related_name="comments", on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE,blank=True,null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    comment = models.CharField(max_length=500)
    created_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{}-{}".format(self.user.username,self.project.name)

class ProjectHours(models.Model):
    project = models.ForeignKey(Project,related_name='projecthours',on_delete=models.CASCADE)
    J = 1
    F = 2
    M = 3
    AP = 4
    MA = 5
    JN = 6
    JL = 7
    AG = 8
    S = 9
    O = 10
    N = 11
    D = 12
    MONTH_CHOICES = [
    (J, 'January'),
    (F, 'February'),
    (M, 'March'),
    (AP, 'April'),
    (MA, 'May'),
    (JN, 'June'),
    (JL, 'July'),
    (AG, 'August'),
    (S, 'September'),
    (O, 'October'),
    (N, 'November'),
    (D, 'December'),
    ]
    month = models.PositiveSmallIntegerField(choices=MONTH_CHOICES)
    year = models.PositiveSmallIntegerField()
    budg_hours = models.PositiveIntegerField()
    created_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User,on_delete=models.CASCADE)


    def __str__(self):
        return '{}-{}'.format(self.project.name,self.month)

class TaskPhotos(models.Model):
    task = models.ForeignKey(Task,on_delete=models.CASCADE)
    project = models.ForeignKey(Project,on_delete=models.CASCADE)
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    A = 'Albaran'
    F = 'Foto'
    FOTO_CHOICES =[
    (A,'Albaran'),
    (F,'Foto'),
    ]
    type = models.CharField(max_length=8,choices=FOTO_CHOICES)
    photo = models.ImageField(upload_to='documents/%Y7%m/%d')
    created_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)

class Festive(models.Model):
    date = models.DateField()
    description = models.CharField(max_length=100)
    created_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User,on_delete=models.CASCADE)


    def __str__(self):
        return self.description

class VehicleComment(models.Model):
    vehicle = models.ForeignKey(Vehicle,related_name="vehicles", on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    comment = models.CharField(max_length=500)
    created_date = models.DateTimeField(auto_now_add=True)
    last_update = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{}-{}".format(self.user.username,self.vehicle.name)
