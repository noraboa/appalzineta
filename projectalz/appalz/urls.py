from django.conf.urls import url
from django.urls import path, include
from appalz import views
from rest_framework.routers import DefaultRouter
from rest_framework import renderers

router = DefaultRouter()
router.register('festives', views.FestiveViewSet)
router.register('vehicles', views.VehicleViewSet)
router.register('clienttypes', views.ClientTypeViewSet)
router.register('absencetypes', views.AbsenceTypeViewSet)
router.register('contracttypes', views.ContractTypeViewSet)
router.register('projecttypes', views.ProjectTypeViewSet)
router.register('projectstatus', views.ProjectStatusViewSet)
router.register('projects', views.ProjectViewSet)
router.register('userprofiles', views.UserProfileViewSet)
router.register('absences', views.AbsenceViewSet)
router.register('favouriteprojects', views.FavouriteProjectViewSet)
router.register('tasktypes',views.TaskTypeViewSet)
router.register('workingdayrecord', views.WorkingDayRecordViewSet)
router.register('tasks', views.TaskViewSet)
router.register('comments', views.CommentViewSet)
router.register('projectshours', views.ProjectHourViewSet)
router.register('contactpersons', views.ContactPersonViewSet)
router.register('clients', views.ClientViewSet)
router.register('vehiclecomments',views.VehicleCommentViewSet)
router.register('users',views.UserViewSet)
router.register('allrecords',views.AllRecordsViewSet)

app_name = 'appalz'

urlpatterns = [
    url('',include(router.urls)),
    url('managers/$', views.IsManagerViewSet.as_view({'get': 'list'}), name='manager-list'),
    url('monthlyrecord/$',views.MonthlyRecordViewSet.as_view({'get':'list'}), name='monthly-record'),
    url('customfestives/$', views.FestiveViewSet.as_view({'post': 'extractdates'})),
    url('allmonthrec/$',views.AllMonthlyRecordViewSet.as_view({'get':'list'}), name='all-records')
]
