from django.contrib import admin
from appalz.models import Project,Task,WorkingDayRecord,UserProfile,Absence,Vehicle,ContactPerson,Comment,Client,Festive,AbsenceType,ContractType,ProjectType,ProjectStatus,ClientType,ProjectHours,TaskType,FavouriteClient,FavouriteProject
from appalz import models
# Register your models here.

admin.site.register(Project)
admin.site.register(Task)
admin.site.register(WorkingDayRecord)
admin.site.register(UserProfile)
admin.site.register(Absence)
admin.site.register(Vehicle)
admin.site.register(ContactPerson)
admin.site.register(Comment)
admin.site.register(Client)
admin.site.register(Festive)
admin.site.register(AbsenceType)
admin.site.register(ContractType)
admin.site.register(ProjectType)
admin.site.register(ProjectStatus)
admin.site.register(ClientType)
admin.site.register(ProjectHours)
admin.site.register(TaskType)
admin.site.register(FavouriteClient)
admin.site.register(FavouriteProject)
admin.site.register(models.VehicleComment)
