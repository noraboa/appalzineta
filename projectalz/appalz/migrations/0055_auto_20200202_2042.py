# Generated by Django 3.0.2 on 2020-02-02 19:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appalz', '0054_auto_20200202_2039'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projecthours',
            name='month',
            field=models.PositiveSmallIntegerField(choices=[('January', 'January'), (2, 'February'), ('March', 'March'), ('April', 'April'), ('May', 'May'), ('June', 'June'), ('July', 'July'), ('August', 'August'), ('September', 'September'), ('October', 'October'), ('November', 'November'), ('December', 'December')]),
        ),
    ]
