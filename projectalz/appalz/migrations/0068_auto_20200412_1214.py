# Generated by Django 3.0.2 on 2020-04-12 10:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('appalz', '0067_auto_20200412_1212'),
    ]

    operations = [
        migrations.RenameField(
            model_name='comment',
            old_name='published_date',
            new_name='created_date',
        ),
        migrations.AlterField(
            model_name='comment',
            name='project',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='comments', to='appalz.Proyecto'),
        ),
    ]
