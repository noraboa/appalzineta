# Generated by Django 3.0.2 on 2020-01-31 16:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appalz', '0038_auto_20200131_1707'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ausencia',
            name='status',
            field=models.CharField(choices=[('P', 'Pending'), ('C', 'Confirmed'), ('D', 'Denied')], max_length=15),
        ),
    ]
