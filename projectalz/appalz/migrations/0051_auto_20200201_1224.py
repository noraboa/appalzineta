# Generated by Django 3.0.2 on 2020-02-01 11:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appalz', '0050_auto_20200201_1220'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projecthours',
            name='month',
            field=models.CharField(choices=[('January', 'January'), ('February', 'February'), ('March', 'March'), ('April', 'April'), ('May', 'May'), ('June', 'June'), ('July', 'July'), ('August', 'August'), ('September', 'September'), ('October', 'October'), ('November', 'November'), ('December', 'December')], max_length=15),
        ),
    ]
