# Generated by Django 3.0.2 on 2020-01-11 19:29

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('appalz', '0015_auto_20200111_2015'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ausencia',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 11, 19, 29, 42, 772593, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='cliente',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 11, 19, 29, 42, 767596, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='comentarios',
            name='published_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 11, 19, 29, 42, 773592, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='fotostarea',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 11, 19, 29, 42, 793580, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='horas',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 11, 19, 29, 42, 774591, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='personacontacto',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 11, 19, 29, 42, 773592, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='proyecto',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 11, 19, 29, 42, 768595, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='registrojornada',
            name='date',
            field=models.DateField(default=datetime.datetime(2020, 1, 11, 19, 29, 42, 768595, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tarea',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 11, 19, 29, 42, 769594, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='startdate',
            field=models.DateField(default=datetime.datetime(2020, 1, 11, 19, 29, 42, 766596, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='vehiculo',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 11, 19, 29, 42, 769594, tzinfo=utc)),
        ),
    ]
