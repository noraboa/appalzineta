# Generated by Django 3.0.2 on 2020-01-31 18:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('appalz', '0044_auto_20200131_1936'),
    ]

    operations = [
        migrations.AlterField(
            model_name='proyecto',
            name='status',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='appalz.ProjectStatus'),
        ),
        migrations.AlterField(
            model_name='proyecto',
            name='type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='appalz.ProjectType'),
        ),
    ]
