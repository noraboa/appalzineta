# Generated by Django 3.0.2 on 2020-04-12 10:26

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('appalz', '0068_auto_20200412_1214'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Proyecto',
            new_name='Project',
        ),
    ]
