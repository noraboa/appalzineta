# Generated by Django 3.0.2 on 2020-04-25 15:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appalz', '0074_project_start_date'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofile',
            name='hol_enjoyed',
        ),
    ]
