# Generated by Django 3.0.2 on 2020-05-16 17:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appalz', '0080_remove_projecthours_real_hours'),
    ]

    operations = [
        migrations.AddField(
            model_name='vehicle',
            name='itv_date',
            field=models.DateField(blank=True, null=True),
        ),
    ]
