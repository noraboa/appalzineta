# Generated by Django 3.0.2 on 2020-04-25 16:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appalz', '0075_remove_userprofile_hol_enjoyed'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='userprofile',
            name='pmat_enjoyed',
        ),
    ]
