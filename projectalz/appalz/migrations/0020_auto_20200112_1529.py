# Generated by Django 3.0.2 on 2020-01-12 14:29

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('appalz', '0019_auto_20200112_0920'),
    ]

    operations = [
        migrations.CreateModel(
            name='Festivo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('description', models.CharField(max_length=100)),
                ('created_date', models.DateTimeField(default=datetime.datetime(2020, 1, 12, 14, 29, 51, 215492, tzinfo=utc))),
            ],
        ),
        migrations.AlterField(
            model_name='ausencia',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 12, 14, 29, 51, 194522, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='cliente',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 12, 14, 29, 51, 190524, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='comentarios',
            name='published_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 12, 14, 29, 51, 196521, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='fotostarea',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 12, 14, 29, 51, 214491, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='horas',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 12, 14, 29, 51, 196521, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='personacontacto',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 12, 14, 29, 51, 195522, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='proyecto',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 12, 14, 29, 51, 191524, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='registrojornada',
            name='date',
            field=models.DateField(default=datetime.datetime(2020, 1, 12, 14, 29, 51, 190524, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='tarea',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 12, 14, 29, 51, 192523, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='startdate',
            field=models.DateField(default=datetime.datetime(2020, 1, 12, 14, 29, 51, 188525, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='vehiculo',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2020, 1, 12, 14, 29, 51, 191524, tzinfo=utc)),
        ),
    ]
