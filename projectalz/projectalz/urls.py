from django.contrib import admin
from django.urls import path
from django.conf.urls import url,include
from django.views.generic import RedirectView
from django.contrib.auth import views as auth_views
from appalz import views
from django.conf import settings
from django.conf.urls.static import static
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'',include('appalz.urls')),
    url(r'accounts/login/$',views.UserLogin,name='login'),
    url(r'accounts/logout/$',views.UserLogout,name='logout'),
    url(r'^favicon\.ico$',RedirectView.as_view(url='/static/images/favicon.ico')),
    path('api/token', views.MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/refresh', TokenRefreshView.as_view(), name='token_refresh'),
    url('api/auth/user',views.LoggedInUserView.as_view({'get': 'list'}),name='loggedin_user'),
]  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
